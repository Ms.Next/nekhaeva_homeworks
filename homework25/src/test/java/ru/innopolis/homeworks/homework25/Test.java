package ru.innopolis.homeworks.homework25;

import java.util.Arrays;
import java.util.List;

public class Test {
    private final static ProductRepository productRepository = new ProductRepositoryJdbcImpl(Main.dataSource);

    public static void main(String[] args) {
        Product product0 = new Product("Упаковка бумаги", 250d);
        Product product1 = new Product("Скотч", 100d);
        Product product2 = new Product("Карандаш", 22d);
        Product product3 = new Product("Ручка", 51d);
        Product product4 = new Product("Цветные мелки", 160d);
        Product product5 = new Product("Дырокол", 100d);

        List<Product> expectedProducts = Arrays.asList(product0, product1, product2, product3, product4, product5);
        findAllTest(expectedProducts);

        double price = 100;
        expectedProducts = Arrays.asList(product1, product5);
        findByPriceTest(expectedProducts, price);

        int ordersCount = 4;
        expectedProducts = Arrays.asList(product1, product2);
        findByOrdersCountTest(expectedProducts, ordersCount);
    }

    private static void findAllTest(List<Product> expectedProducts) {
        List<Product> actualProducts = productRepository.findAll();
        checkAndPrintResult(expectedProducts, actualProducts, "findAllTest");
    }

    private static void findByPriceTest(List<Product> expectedProducts, double price) {
        List<Product> actualProducts = productRepository.findByPrice(price);
        checkAndPrintResult(expectedProducts, actualProducts, "findByPriceTest");
    }

    private static void findByOrdersCountTest(List<Product> expectedProducts, int ordersCount) {
        List<Product> actualProducts = productRepository.findByOrdersCount(ordersCount);
        checkAndPrintResult(expectedProducts, actualProducts, "findByOrdersCountTest");
    }

    private static void checkAndPrintResult(List<Product> expectedProducts, List<Product> actualProducts, String testName) {
        if (expectedProducts.equals(actualProducts)) {
            System.out.println("\nТест " + testName + " успешно прошёл.");
        } else {
            System.out.println("\nТест " + testName + " провалился.");
        }

        System.out.println("Ожидаемый список товаров: " + expectedProducts);
        System.out.println("Фактический список товаров: " + actualProducts);
    }
}
