package ru.innopolis.homeworks.homework25;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductRepositoryJdbcImpl implements ProductRepository {

    private final JdbcTemplate jdbcTemplate;

    private final RowMapper<Product> rowMapper = (rs, rowNum) -> Product.builder()
            .id(rs.getInt("id"))
            .description(rs.getString("description"))
            .price(rs.getDouble("price"))
            .build();

    public ProductRepositoryJdbcImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Product> findAll() {
        String SQL_SELECT_ALL = "select * from product;";
        return jdbcTemplate.query(SQL_SELECT_ALL, rowMapper);
    }

    @Override
    public List<Product> findByPrice(double price) {
        String SQL_SELECT_BY_PRICE = "select * from product where price = ?;";
        return jdbcTemplate.query(SQL_SELECT_BY_PRICE, rowMapper, price);
    }

    @Override
    public List<Product> findByOrdersCount(int ordersCount) {
        String SQL_SELECT_BY_ORDERS_COUNT = "select p.* from product p left join request r on p.id = r.product_id  group by p.id having count(*) = ? order by p.id;";
        return jdbcTemplate.query(SQL_SELECT_BY_ORDERS_COUNT, rowMapper, ordersCount);
    }
}
