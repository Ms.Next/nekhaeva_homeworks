package ru.innopolis.homeworks.homework25;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.List;

public class Main {
    final static DataSource dataSource = new DriverManagerDataSource(
            "jdbc:postgresql://localhost:5432/products",
            "postgres",
            "postgres");

    public static void main(String[] args) {
        ProductRepository productRepository = new ProductRepositoryJdbcImpl(dataSource);

        List<Product> products = productRepository.findAll();
        printProducts(products, "Все найденные товары:");

        double price = 100;
        products = productRepository.findByPrice(price);
        printProducts(products, "Все товары стоимостью " + price + ":");

        int ordersCount = 4;
        products = productRepository.findByOrdersCount(ordersCount);
        printProducts(products, "Все товары с количеством заказов " + ordersCount + ":");
    }

    private static void printProducts(List<Product> products, String title) {
        System.out.println("\n" + title);
        for (Product product : products) {
            System.out.println(product);
        }
    }
}
