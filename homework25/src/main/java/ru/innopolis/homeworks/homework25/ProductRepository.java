package ru.innopolis.homeworks.homework25;

import java.util.List;

public interface ProductRepository {
    List<Product> findAll();

    List<Product> findByPrice(double price);

    List<Product> findByOrdersCount(int ordersCount);
}
