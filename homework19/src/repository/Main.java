package src.repository;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("homework19/users.txt");
        List<User> users = usersRepository.findAll();
        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        User igor = new User("Игорь", 33, true);
        usersRepository.save(igor);
        System.out.println();
        users = usersRepository.findByAge(25);
        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        System.out.println();
        users = usersRepository.findByIsWorkerIsTrue();
        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
    }
}
