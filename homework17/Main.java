import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String s = "Теперь дети не играют а учатся Они все учатся учатся и никогда не начнут жить Все это так а жаль право жаль";
        String[] words = s.split(" ");
        Map<String, Integer> wordsMap = new HashMap<>();
        for (String word : words) {
            wordsMap.put(word.toLowerCase(), wordsMap.getOrDefault(word.toLowerCase(), 0) + 1);
        }
        for (Map.Entry<String, Integer> entry : wordsMap.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }

}
