public class Rectangle extends Figure {
    private double length;
    private double width;

    Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    double getPerimeter() {
        return length * 2 + width * 2;
    }
}
