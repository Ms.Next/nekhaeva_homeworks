public class Ellipse extends Figure {
    private double radius1;
    private double radius2;

    Ellipse(double radius1, double radius2) {
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    double getPerimeter() {
        return 4 * (Math.PI * radius1 * radius2 + radius1 - radius2) / (radius1 + radius2);
    }
}
