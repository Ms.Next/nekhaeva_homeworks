public class Main {

    public static void main(String[] args) {
        Figure figure = new Figure();
        Square square = new Square(7.5);
        Circle circle = new Circle(5.8);
        Rectangle rectangle = new Rectangle(8.0, 3.5);
        Ellipse ellipse = new Ellipse(1.5, 8.5);

        System.out.println(figure.getPerimeter());
        System.out.println(square.getPerimeter());
        System.out.println(circle.getPerimeter());
        System.out.println(rectangle.getPerimeter());
        System.out.println(ellipse.getPerimeter());
    }
}
