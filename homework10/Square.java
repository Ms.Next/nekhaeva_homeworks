public class Square extends Rectangle implements Movable {

    Square(double length) {
        super(length, length);
    }

    @Override
    public void move(double x, double y) {
        this.x = x;
        this.y = y;
        System.out.println("Квадрат перемещен на координаты x = " + x + ", y = " + y);
    }
}
