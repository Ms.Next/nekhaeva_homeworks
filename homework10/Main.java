public class Main {
    public static void main(String[] args) {
        Movable[] array = new Movable[4];
        Movable square1 = new Square(8.4);
        Movable square2 = new Square(3.4);
        Movable circle1 = new Circle(2.5);
        Movable circle2 = new Circle(6.7);
        array[0] = square1;
        array[1] = square2;
        array[2] = circle1;
        array[3] = circle2;
        for (int i = 0; i < array.length; i++) {
            array[i].move(12.4, 9.3);
        }
    }
}
