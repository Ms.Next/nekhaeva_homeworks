public class Circle extends Ellipse implements Movable {

    Circle(double radius) {
        super(radius, radius);
    }

    @Override
    public void move(double x, double y) {
        this.x = x;
        this.y = y;
        System.out.println("Круг перемещен на координаты x = " + x + ", y = " + y);
    }
}
