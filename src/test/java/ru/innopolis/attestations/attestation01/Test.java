package ru.innopolis.attestations.attestation01;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Test {

    private final static UsersRepository usersRepository = new UsersRepositoryFileImpl("src/test/resources/users.txt");

    public static void main(String[] args) {
        usersRepository.deleteAll();

        User user0 = new User(0, "Марсель", 27, true);
        User user1 = new User(1, "Максим", 22, false);
        User user2 = new User(2, "Виктор", 25, false);

        List<User> expectedUsers = Arrays.asList(user0, user1, user2);
        for (User user : expectedUsers) {
            usersRepository.save(user);
        }

        findAllTest(expectedUsers);

        findByAgeTest(Collections.singletonList(user1), 22);
        findByAgeTest(Collections.singletonList(user2), 25);

        findByIsWorkerTest(Collections.singletonList(user0));

        findByIdTest(0, user0);
        findByIdTest(1, user1);

        User user = new User(3, "Игорь", 33, true);
        saveTest(user);

        user.setName("Разиль");
        user.setAge(24);
        user.setWorker(false);
        updateTest(user);
    }

    private static void findAllTest(List<User> expectedUsers) {
        List<User> actualUsers = usersRepository.findAll();
        checkAndPrintResult(expectedUsers, actualUsers, "findAllTest");
    }

    private static void findByAgeTest(List<User> expectedUsers, int age) {
        List<User> actualUsers = usersRepository.findByAge(age);
        checkAndPrintResult(expectedUsers, actualUsers, "findByAgeTest");
    }

    private static void findByIsWorkerTest(List<User> expectedUsers) {
        List<User> actualUsers = usersRepository.findByIsWorker();
        checkAndPrintResult(expectedUsers, actualUsers, "findByIsWorkerTest");
    }

    private static void findByIdTest(int id, User expectedUser) {
        User actualUser = usersRepository.findById(id);
        checkAndPrintResult(Collections.singletonList(expectedUser), Collections.singletonList(actualUser), "findByIdTest");
    }

    private static void saveTest(User user) {
        List<User> expectedUsers = usersRepository.findAll();
        expectedUsers.add(user);

        usersRepository.save(user);
        List<User> actualUsers = usersRepository.findAll();

        checkAndPrintResult(expectedUsers, actualUsers, "saveTest");
    }

    private static void updateTest(User user) {
        List<User> expectedUsers = usersRepository.findAll();
        User foundUser = usersRepository.findById(user.getId());
        expectedUsers.set(expectedUsers.indexOf(foundUser), user);

        usersRepository.update(user);
        List<User> actualUsers = usersRepository.findAll();

        checkAndPrintResult(expectedUsers, actualUsers, "updateTest");
    }

    private static void checkAndPrintResult(List<User> expectedUsers, List<User> actualUsers, String testName) {
        String result = "провалился";
        if (expectedUsers.equals(actualUsers)) {
            result = "успешно прошёл";
        }
        System.out.println("Тест '" + testName + "' " + result + ".\nОжидаемый список пользователей:");
        Main.printUsers(expectedUsers);
        System.out.println("Фактический список пользователей:");
        Main.printUsers(actualUsers);
        System.out.println();
    }
}
