package ru.innopolis.homeworks.homework19;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class Test {

    private static final Logger LOGGER = Logger.getLogger(Test.class.getName());
    private final static UsersRepository usersRepository = new UsersRepositoryFileImpl("src/test/resources/users.txt");

    public static void main(String[] args) {
        usersRepository.deleteAll();

        User user0 = new User("Марсель", 27, true);
        User user1 = new User("Максим", 22, false);
        User user2 = new User("Виктор", 25, false);

        List<User> expectedUsers = Arrays.asList(user0, user1, user2);
        for (User user : expectedUsers) {
            usersRepository.save(user);
        }

        findAllTest(expectedUsers);

        int age = 25;
        expectedUsers = Collections.singletonList(user2);
        findByAgeTest(expectedUsers, age);

        expectedUsers = Collections.singletonList(user0);
        findByIsWorkerTest(expectedUsers);

        User igor = new User("Игорь", 33, true);
        saveTest(igor);
    }

    private static void findAllTest(List<User> expectedUsers) {
        List<User> actualUsers = usersRepository.findAll();
        checkAndPrintResult(expectedUsers, actualUsers, "findAllTest");
    }

    private static void findByAgeTest(List<User> expectedUsers, int age) {
        List<User> actualUsers = usersRepository.findByAge(age);
        checkAndPrintResult(expectedUsers, actualUsers, "findByAgeTest");
    }

    private static void findByIsWorkerTest(List<User> expectedUsers) {
        List<User> actualUsers = usersRepository.findByIsWorker();
        checkAndPrintResult(expectedUsers, actualUsers, "findByIsWorkerTest");
    }

    private static void saveTest(User user) {
        List<User> expectedUsers = usersRepository.findAll();
        expectedUsers.add(user);

        usersRepository.save(user);
        List<User> actualUsers = usersRepository.findAll();

        checkAndPrintResult(expectedUsers, actualUsers, "saveTest");
    }

    private static void checkAndPrintResult(List<User> expectedUsers, List<User> actualUsers, String testName) {
        if (expectedUsers.equals(actualUsers)) {
            System.out.println("Тест " + testName + " успешно прошёл.");
        } else {
            System.out.println("Тест " + testName + " провалился.");
        }

        System.out.println("Ожидаемый список пользователей: " + expectedUsers);
        System.out.println("Фактический список пользователей: " + actualUsers);
        System.out.println();
    }
}
