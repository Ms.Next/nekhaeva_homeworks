package ru.innopolis.homeworks.homework21;

public class Test {

    public static void main(String[] args) {
        int threadCount = 3;
        int expectedSum = 34;
        Main.array = new int[]{3, 6, 2, 8, 1, 9, 5};

        getSumWithoutThreadsTest(expectedSum);
        getSumWithThreadsTest(threadCount, expectedSum);
    }

    public static void getSumWithoutThreadsTest(int expected) {
        System.out.println("\nТест 'getSumWithoutThreadsTest' запущен.");
        compareSums(expected, Main.getSum(Main.array));
    }

    public static void getSumWithThreadsTest(int threadCount, int expected) {
        System.out.println("\nТест 'getSumWithThreadsTest' запущен.");
        Main.sums = new int[threadCount];
        Main.computeSums();
        compareSums(expected, Main.getSum(Main.sums));
    }

    private static void compareSums(int expected, int actual) {
        if (expected == actual) {
            System.out.println("фактическая сумма совпадает с ожидаемой, которая равна " + actual + ".");
        } else {
            System.err.println("фактическая сумма, которая равна " + expected + ", не совпадает с ожидаемой, которая равна "
                    + actual + ".");
        }
    }
}
