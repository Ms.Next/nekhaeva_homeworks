create table product
(
    id          serial primary key,
    description varchar(100),
    price       numeric,
    quantity    integer
);

create table client
(
    id   serial primary key,
    name varchar(25)
);

create table request
(
    id         serial primary key,
    product_id integer,
    client_id  integer,
    date       date,
    quantity   integer,
    foreign key (product_id) references product (id),
    foreign key (client_id) references client (id)
);

insert into product (description, price, quantity) values ('Упаковка бумаги', 250, 10);
insert into product (description, price, quantity) values ('Скотч', 100, 2);
insert into product (description, price, quantity) values ('Карандаш', 22, 1);
insert into product (description, price, quantity) values ('Ручка', 51, 5);
insert into product (description, price, quantity) values ('Цветные мелки', 160, 1);
insert into product (description, price, quantity) values ('Дырокол', 100, 1);

insert into client (name) values ('Канцелярия');
insert into client (name) values ('Эконика');
insert into client (name) values ('Свобода');

insert into request (product_id, client_id, date, quantity) values (1, 2, '10.01.2021', 1);
insert into request (product_id, client_id, date, quantity) values (2, 1, '11.01.2021', 2);
insert into request (product_id, client_id, date, quantity) values (3, 2, '12.01.2021', 10);
insert into request (product_id, client_id, date, quantity) values (4, 3, '11.01.2021', 3);
insert into request (product_id, client_id, date, quantity) values (3, 1, '10.01.2021', 20);
insert into request (product_id, client_id, date, quantity) values (3, 2, '12.01.2021', 5);
insert into request (product_id, client_id, date, quantity) values (2, 2, '11.01.2021', 3);
insert into request (product_id, client_id, date, quantity) values (2, 3, '10.01.2021', 8);
insert into request (product_id, client_id, date, quantity) values (4, 1, '11.01.2021', 2);
insert into request (product_id, client_id, date, quantity) values (3, 2, '12.01.2021', 1);
insert into request (product_id, client_id, date, quantity) values (2, 3, '12.01.2021', 6);

--получить id клиента и количество сделанных им заказов с сортировкой по дате заказа
select client_id, quantity
from request
order by date;

-- получить название компании с id = 1 и количество оформленных этой компанией заказов за всё время
select name, (select count(*) from request where client_id = 1) as request_count
from client
where id = 1;
-- вариант с испльзованием join
select c.name, count(*) as request_count
from client c
         right join request r on c.id = r.client_id
where c.id = 1
group by c.name;

-- получить id продукта, количество заказов которого превышает 3
select product_id
from request
group by product_id
having count(*) > 3;

-- получить общее количество товаров
select sum(quantity)
from product;
