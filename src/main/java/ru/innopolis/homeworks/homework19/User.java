package ru.innopolis.homeworks.homework19;

import java.util.Objects;

public class User {
    private String name;
    private int age;
    private boolean isWorker;

    public User(String name, int age, boolean isWorker) {
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public void setWorker(boolean worker) {
        isWorker = worker;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        User that = (User) object;
        boolean equalsAge = this.age == that.age;
        boolean equalsIsWorker = this.isWorker == that.isWorker;
        boolean equalsName = Objects.equals(this.name, that.name);
        return equalsName && equalsAge && equalsIsWorker;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, isWorker);
    }

    @Override
    public String toString() {
        return this.name + " - возраст " + this.age + " - " + (this.isWorker ? "" : "не") + "работающий";
    }
}
