package ru.innopolis.homeworks.homework19;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private final String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                User user = deserialize(line);
                users.add(user);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> allUsers = findAll();
        List<User> foundUsers = new ArrayList<>();
        for (User user : allUsers) {
            if (user.getAge() == age) {
                foundUsers.add(user);
            }
        }
        return foundUsers;
    }

    @Override
    public List<User> findByIsWorker() {
        List<User> allUsers = findAll();
        List<User> foundUsers = new ArrayList<>();
        for (User user : allUsers) {
            if (user.isWorker()) {
                foundUsers.add(user);
            }
        }
        return foundUsers;
    }

    @Override
    public void save(User user) {
        save(Collections.singletonList(user), true);
    }

    @Override
    public void deleteAll() {
        if (findAll().size() == 0) {
            return;
        }
        save(Collections.emptyList(), false);
    }

    private void save(List<User> users, boolean append) {
        String data = serialize(users);
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, append))) {
            bufferedWriter.write(data);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private String serialize(List<User> users) {
        StringBuilder stringBuilder = new StringBuilder();
        users.forEach(user -> stringBuilder
                .append(user.getName())
                .append("|")
                .append(user.getAge())
                .append("|")
                .append(user.isWorker())
                .append("\n"));
        return stringBuilder.toString();
    }

    private User deserialize(String line) {
        String[] columns = line.split("\\|");

        if (columns.length != 3) {
            throw new IllegalArgumentException("Не удалось распознать строку '" + line
                    + "' в файле с данными о пользователях " + fileName + ".");
        }

        String name = columns[0];

        int age;
        String ageColumn = columns[1];
        try {
            age = Integer.parseInt(ageColumn);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Не удалось распознать возраст '" + ageColumn
                    + "' как целое число в строке '" + line + "'.");
        }

        boolean isWorker = Boolean.parseBoolean(columns[2]);

        return new User(name, age, isWorker);
    }
}
