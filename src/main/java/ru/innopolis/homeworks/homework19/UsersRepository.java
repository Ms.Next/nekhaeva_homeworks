package ru.innopolis.homeworks.homework19;

import java.util.List;

public interface UsersRepository {
    List<User> findAll();

    List<User> findByAge(int age);

    List<User> findByIsWorker();

    void save(User user);

    void deleteAll();
}
