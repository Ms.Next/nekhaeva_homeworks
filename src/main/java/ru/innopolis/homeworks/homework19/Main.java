package ru.innopolis.homeworks.homework19;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("src/main/resources/users.txt");
        usersRepository.deleteAll();

        usersRepository.save(new User("Марсель", 27, true));
        usersRepository.save(new User("Максим", 22, false));
        usersRepository.save(new User("Виктор", 25, false));

        List<User> users = usersRepository.findAll();
        System.out.println("Все найденные пользователи в начале программы:");
        printUsers(users);

        int age = 25;
        users = usersRepository.findByAge(age);
        System.out.println("\nВсе найденные пользователи c возрастом " + age + ":");
        printUsers(users);

        users = usersRepository.findByIsWorker();
        System.out.println("\nВсе найденные пользователи, которые работают:");
        printUsers(users);

        User igor = new User("Игорь", 33, true);
        usersRepository.save(igor);

        users = usersRepository.findAll();
        System.out.println("\nВсе найденные пользователи после добавления нового пользователя:");
        printUsers(users);
    }

    private static void printUsers(List<User> users) {
        for (User user : users) {
            System.out.println(user);
        }
    }
}
