package ru.innopolis.homeworks.homework21;

public class SumThread extends Thread {
    private final int number;
    private final int from;
    private final int to;

    public SumThread(int number, int from, int to) {
        this.number = number;
        this.from = from;
        this.to = to;
    }

    @Override
    public void run() {
        for (int i = from; i < to; i++) {
            Main.sums[number] += Main.array[i];
        }
    }
}
