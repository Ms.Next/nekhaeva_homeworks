package ru.innopolis.homeworks.homework21;

import java.util.Random;
import java.util.Scanner;

public class Main {
    static int[] array;
    static int[] sums;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numbersCount = getParam(scanner, "количество чисел в массиве");
        int threadsCount = getParam(scanner, "количество потоков");
        scanner.close();

        array = new int[numbersCount];
        fillArray();

        sums = new int[threadsCount];
        computeSums();

        int realSum = getSum(array);
        System.out.println("Сумма чисел в массиве, вычисленная без использования многопоточности:\n" + realSum + ".");

        int byThreadsSum = getSum(sums);
        System.out.println("Сумма чисел в массиве, вычисленная c использованием многопоточности (число потоков "
                + threadsCount + "):\n" + byThreadsSum + ".");
    }

    static void computeSums() {
        int bound = 0;
        int numbersCount = array.length;
        int threadsCount = sums.length;
        int modulo = numbersCount % threadsCount;
        int capacity = numbersCount / threadsCount + 1;

        Thread[] threads = new Thread[threadsCount];

        for (int i = 0; i < modulo; i++) {
            threads[i] = new SumThread(i, bound, bound += capacity);
        }

        capacity--;

        for (int i = modulo; i < threadsCount; i++) {
            threads[i] = new SumThread(i, bound, bound += capacity);
        }

        startThreads(threads);
        joinThreads(threads);
    }

    static int getSum(int[] array) {
        int sum = 0;
        for (int number : array) {
            sum += number;
        }
        return sum;
    }

    private static int getParam(Scanner scanner, String paramName) {
        int number = 0;
        while (number < 1) {
            System.out.print("Введите " + paramName + " (число > 0): ");
            try {
                number = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException ignored) {
            }
        }
        return number;
    }

    private static void fillArray() {
        Random random = new Random();
        int length = array.length;
        for (int i = 0; i < length; i++) {
            array[i] = random.nextInt(length);
        }
    }

    private static void startThreads(Thread[] threads) {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    private static void joinThreads(Thread[] threads) {
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
