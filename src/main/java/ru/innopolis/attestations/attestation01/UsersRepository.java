package ru.innopolis.attestations.attestation01;

import java.util.List;

public interface UsersRepository {
    List<User> findAll();

    List<User> findByAge(int age);

    List<User> findByIsWorker();

    User findById(int id);

    void save(User user);

    void update(User user);

    void deleteAll();
}
