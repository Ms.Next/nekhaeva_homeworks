package ru.innopolis.attestations.attestation01;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("src/main/resources/users.txt");
        usersRepository.deleteAll();

        usersRepository.save(new User(0, "Марсель", 27, true));
        usersRepository.save(new User(1, "Максим", 22, false));
        usersRepository.save(new User(2, "Виктор", 25, false));

        List<User> users = usersRepository.findAll();
        System.out.println("Все найденные пользователи в начале программы:");
        printUsers(users);

        int age = 25;
        users = usersRepository.findByAge(age);
        System.out.println("\nВсе найденные пользователи c возрастом " + age + ":");
        printUsers(users);

        users = usersRepository.findByIsWorker();
        System.out.println("\nВсе найденные пользователи, которые работают:");
        printUsers(users);

        int id = 1;
        User user = usersRepository.findById(id);
        System.out.println("\nНайденный пользователь с идентификатором " + id + ":\n" + user);

        user = new User(3, "Игорь", 33, true);
        usersRepository.save(user);

        users = usersRepository.findAll();
        System.out.println("\nВсе найденные пользователи после добавления нового пользователя:");
        printUsers(users);

        user.setName("Разиль");
        user.setAge(24);
        user.setWorker(false);
        usersRepository.update(user);
        users = usersRepository.findAll();
        System.out.println("\nВсе найденные пользователи после обновления характеристик пользователя с идентификатором "
                + user.getId() + ":");
        printUsers(users);
    }

    static void printUsers(List<User> users) {
        for (User user : users) {
            System.out.println(user);
        }
    }
}
