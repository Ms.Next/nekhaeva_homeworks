package ru.innopolis.attestations.attestation01;

import java.util.Objects;

public class User {
    private int id;
    private String name;
    private int age;
    private boolean isWorker;

    public User(int id, String name, int age, boolean isWorker) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public void setWorker(boolean worker) {
        isWorker = worker;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        User that = (User) object;
        boolean equalsId = this.id == that.id;
        boolean equalsAge = this.age == that.age;
        boolean equalsIsWorker = this.isWorker == that.isWorker;
        boolean equalsName = Objects.equals(this.name, that.name);
        return equalsId && equalsName && equalsAge && equalsIsWorker;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age, isWorker);
    }

    @Override
    public String toString() {
        return "пользователь " + this.id + " - " + this.name + " - возраст " + this.age + " - " + (this.isWorker ? "" : "не") + "работающий";
    }
}
