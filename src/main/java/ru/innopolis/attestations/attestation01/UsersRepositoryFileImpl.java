package ru.innopolis.attestations.attestation01;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl implements UsersRepository {

    private final String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                User user = deserialize(line);
                users.add(user);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public List<User> findByAge(int age) {
        return findAll()
                .stream()
                .filter(user -> user.getAge() == age)
                .collect(Collectors.toList());
    }

    @Override
    public List<User> findByIsWorker() {
        return findAll()
                .stream()
                .filter(User::isWorker)
                .collect(Collectors.toList());
    }

    @Override
    public User findById(int id) {
        List<User> users = findAll()
                .stream()
                .filter(user -> user.getId() == id)
                .collect(Collectors.toList());

        int size = users.size();
        if (size < 1) {
            throw new IllegalArgumentException("Пользователь с идентификатором " + id + " не найден.");
        }
        if (size > 1) {
            throw new IllegalArgumentException("Идентификатор " + id + " не является уникальным," +
                    " количество найденных пользователей: " + size + ".");
        }

        return users.get(0);
    }

    @Override
    public void save(User user) {
        save(Collections.singletonList(user), true);
    }

    @Override
    public void update(User user) {
        List<User> users = findAll();
        User foundUser = findById(user.getId());
        users.set(users.indexOf(foundUser), user);
        save(users, false);
    }

    @Override
    public void deleteAll() {
        if (findAll().size() == 0) {
            return;
        }
        save(Collections.emptyList(), false);
    }

    private void save(List<User> users, boolean append) {
        String data = serialize(users);
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, append))) {
            bufferedWriter.write(data);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private String serialize(List<User> users) {
        StringBuilder stringBuilder = new StringBuilder();
        users.forEach(user -> stringBuilder
                .append(user.getId())
                .append("|")
                .append(user.getName())
                .append("|")
                .append(user.getAge())
                .append("|")
                .append(user.isWorker())
                .append("\n"));
        return stringBuilder.toString();
    }

    private User deserialize(String line) {
        String[] columns = line.split("\\|");

        if (columns.length != 4) {
            throw new IllegalArgumentException("Не удалось распознать строку '" + line
                    + "' в файле с данными о пользователях " + fileName + ".");
        }

        int id = parseInt("идентификатор", columns[0], line);
        String name = columns[1];
        int age = parseInt("возраст", columns[2], line);
        boolean isWorker = Boolean.parseBoolean(columns[3]);

        return new User(id, name, age, isWorker);
    }

    private int parseInt(String columnName, String columnValue, String line) {
        try {
            return Integer.parseInt(columnValue);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Не удалось распознать " + columnName + " '" + columnValue
                    + "' как целое число в строке '" + line + "'.");
        }
    }
}
