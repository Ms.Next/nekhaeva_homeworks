package ru.innopolis.homeworks;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName(value = "NumbersUtil работает, когда")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    @Nested
    @DisplayName("gcd() работает")
    public class ForSum {
        @ParameterizedTest(name = "возвращает {2}, если {2} - это НОД {0} и {1}")
        @CsvSource(value = {"120, 36, 12", "24, 16, 8", "4, 22, 2"})
        public void возвращает_корректный_НОД(int a, int b, int result) {
            assertEquals(result, NumbersUtil.gcd(a, b));
        }
    }
}
