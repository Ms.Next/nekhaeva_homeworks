package ru.innopolis.homeworks;

public class Main {
    public static void main(String[] args) {
        int a = 120;
        int b = 36;
        int result = NumbersUtil.gcd(a, b);
        System.out.println("Наибольший общий делитель чисел " + a + " и " + b + " равен " + result + ".");
    }
}
