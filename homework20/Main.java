import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        List<Map<String, String>> cars = new ArrayList<>();

        try {
            new BufferedReader(new FileReader("homework20/file.txt")).lines().forEach(e -> {
                HashMap<String, String> map = new HashMap<>();
                String[] params = e.split("\\|");
                map.put("number", params[0]);
                map.put("model", params[1]);
                map.put("color", params[2]);
                map.put("km", params[3]);
                map.put("price", params[4]);
                cars.add(map);
            });
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException();
        }

        // 1
        cars.stream()
                .filter(e -> e.get("color").equals("Black") || e.get("km").equals("0"))
                .map(e -> e.get("number"))
                .forEach(e -> System.out.println(e));

        // 2
        long count = cars.stream()
                .map(e -> Integer.parseInt(e.get("price")))
                .filter(e -> e >= 700 && e <= 800)
                .distinct()
                .count();
        System.out.println(count);

        // 3
        String color = cars.stream()
                .min((a, b) -> Integer.parseInt(a.get("price")) - Integer.parseInt(b.get("price")))
                .map(e -> e.get("color"))
                .orElse(null);
        System.out.println(color);

        // 4
        List<Integer> list = cars
                .stream()
                .filter(e -> e.get("model").equals("Camry"))
                .map(e -> Integer.parseInt(e.get("price")))
                .collect(Collectors.toList());
        long average = list.stream().reduce((a, b) -> a + b).orElse(0) / list.size();
        System.out.println(average);
    }
}

