package ru.innopolis.attestations.attestation03.repositories;

import ru.innopolis.attestations.attestation03.models.Product;

import java.util.List;

public interface ProductRepository {
    List<Product> findAll();

    void save(Product product);

    void delete(int id);
}
