package ru.innopolis.attestations.attestation03.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.Objects;

@Builder
@Getter
@AllArgsConstructor
public class Product {
    private Integer id;
    private String description;
    private Double price;
    private Integer quantity;

    public Product(String description, Double price, Integer quantity) {
        this.description = description;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || this.getClass() != object.getClass()) {
            return false;
        }
        Product that = (Product) object;
        return Objects.equals(this.description, that.description)
                && Objects.equals(this.price, that.price)
                && Objects.equals(this.quantity, that.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, price, quantity);
    }

    @Override
    public String toString() {
        return description + " - стоимость " + price + " - количество " + price;
    }
}
