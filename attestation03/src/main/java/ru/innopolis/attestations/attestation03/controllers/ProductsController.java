package ru.innopolis.attestations.attestation03.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import ru.innopolis.attestations.attestation03.models.Product;
import ru.innopolis.attestations.attestation03.repositories.ProductRepository;

@Controller
public class ProductsController {
    private final ProductRepository productRepository;

    @Autowired
    public ProductsController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @PostMapping("/products")
    public String addProduct(String description, Double price, Integer quantity) {
        Product product = Product.builder()
                .description(description)
                .price(price)
                .quantity(quantity)
                .build();
        productRepository.save(product);
        return "redirect:/products_add.html";
    }
}
