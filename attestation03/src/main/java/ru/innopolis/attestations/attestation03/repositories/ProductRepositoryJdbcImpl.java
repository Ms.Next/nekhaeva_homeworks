package ru.innopolis.attestations.attestation03.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.innopolis.attestations.attestation03.models.Product;

import javax.sql.DataSource;
import java.util.List;

@Component
public class ProductRepositoryJdbcImpl implements ProductRepository {

    private static final String SQL_SELECT_ALL = "select * from product order by id;";
    private static final String SQL_INSERT = "insert into product (description, price, quantity) values (?, ?, ?)";
    private static final String SQL_DELETE = "delete from product where id = ?;";

    private static final RowMapper<Product> rowMapper = (rs, rowNum) -> Product.builder()
            .id(rs.getInt("id"))
            .description(rs.getString("description"))
            .price(rs.getDouble("price"))
            .quantity(rs.getInt("quantity"))
            .build();

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductRepositoryJdbcImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, rowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getQuantity());
    }

    @Override
    public void delete(int id) {
        jdbcTemplate.update(SQL_DELETE, id);
    }
}
