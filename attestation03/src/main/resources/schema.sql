create table product
(
    id          serial primary key,
    description varchar(100),
    price       numeric,
    quantity    integer
);

insert into product (description, price, quantity) values ('Упаковка бумаги', 250, 10);
insert into product (description, price, quantity) values ('Скотч', 100, 2);
insert into product (description, price, quantity) values ('Карандаш', 22, 1);
insert into product (description, price, quantity) values ('Ручка', 51, 5);
insert into product (description, price, quantity) values ('Цветные мелки', 160, 1);
insert into product (description, price, quantity) values ('Дырокол', 100, 1);
