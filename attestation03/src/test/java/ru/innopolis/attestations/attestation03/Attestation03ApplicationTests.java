package ru.innopolis.attestations.attestation03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.innopolis.attestations.attestation03.controllers.ProductsController;
import ru.innopolis.attestations.attestation03.models.Product;
import ru.innopolis.attestations.attestation03.repositories.ProductRepository;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@DisplayName(value = "Проверяем работу ProductsController")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class Attestation03ApplicationTests {

    private final ProductRepository repository;
    private final ProductsController controller;
    private final List<Product> productsToBeDeleted = new ArrayList<>();

    @Autowired
    public Attestation03ApplicationTests(ProductRepository repository, ProductsController controller) {
        this.repository = repository;
        this.controller = controller;
    }

    @ParameterizedTest(name = "добавление товара \"{0}\" стоимостью {1} руб. в количестве {2} штук")
    @CsvSource(value = {"Скрепка, 10, 3", "Клей, 35, 2", "Линейка, 25, 5", "Скоросшиватель, 17, 50"})
    void add_product(String description, Double price, Integer quantity) {
        Product product = new Product(description, price, quantity);

        List<Product> productsBefore = repository.findAll();
        controller.addProduct(product.getDescription(), product.getPrice(), product.getQuantity());
        List<Product> productsAfter = repository.findAll();

        Assertions.assertAll(
                () -> Assertions.assertEquals(productsBefore.size() + 1, productsAfter.size(),
                        "Число товаров после добавления нового отличается от ожидаемого."),
                () -> Assertions.assertEquals(product, productsAfter.get(productsBefore.size()),
                        "Добавленный товар отличается от ожидаемого."));

        repository.delete(productsAfter.get(productsBefore.size()).getId());
    }
}
